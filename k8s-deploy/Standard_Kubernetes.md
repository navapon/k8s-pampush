################################################################################
# ReadMe: Stanard Format Kubernetes for Opcellent                              #
################################################################################
1. Label for Pods, Deployment, Service, Ingress etc

  labels:
    name:       Name
    owner:      Owner Name with Format: Vendorname_OwnerName: Ex: Capco_Alan
    module:     (Restful/Database/JobQueue/etc)
    tier:       (Frontend/Backend/DMZ/etc)
    release:    (Pre-Alpha, Alpha, Beta, Release-Candidate, General-Avaliable)
    createdate: Date for create this module (YYYYMMDD) Ex: 20180523191003
    updatedate: Last modified this module (YYYYMMDD) Ex: 20180523191003
    
2. Standard Naming of Kubernetes Object:
    <Name of Module> - <Object Type of K8S>
    Ex: OTPMobile-Deployment, FluentdLog-SVC, SCBEASY-Quota

3. Standard for Create Namespace:
    Namespace: CustomerName-Module-namespace: EX: tesco-push-namespace

4. Standard Quota for Namespace:
  spec:
    hard:
      pods: "50"
      services: "20"
      services.nodeports: "10"
      requests.cpu: "4"
      requests.memory: 8Gi
      limits.cpu: "24"
      limits.memory: 64Gi

5. Stanard Limit for Namespace:
spec:
  limits:
  - max:
      cpu: "4"
      memory: 4Gi
    min:
      cpu: 400m
      memory: 100Mi
    type: Pod
  - default:
      cpu: 200m
      memory: 200Mi
    defaultRequest:
      cpu: 200m
      memory: 100Mi
    max:
      cpu: "2"
      memory: 4Gi
    min:
      cpu: 100m
      memory: 50Mi
    type: Container

6. Standard Role of Kubernetes on Production Environment:
    On Production Environment. Kubernetes will separate role for node as below:
    1. Master Node: "kubectl label nodes xxxx role=master"      ==> This for Master Node
    2. Worker Node: "kubectl label nodes xxxx role=worker"      ==> This for Worker Node (In case insufficient, we will label worker togather)
    3. Ingress Node: "kubectl label nodes xxxx role=ingress"    ==> This for Ingress Workload Only

    So. For all yaml file that will deploy on production: Need to set "nodeSelector" with role:worker only like example below:

    xxxx
    spec:
      containers:
        - name: webtest
          image: labdocker/cluster:webservicelite
          ports:
             - xxxxx
      nodeSelector:
        role: worker 