#!/bin/bash

# delete all deployment and service
kubectl delete deployment/push-producer
kubectl delete deployment/push-consumer
kubectl delete deployment/kafka
kubectl delete deployment/zookeeper
kubectl delete deployment/kafka-manager

kubectl delete svc/push-producer
kubectl delete svc/push-consumer
kubectl delete svc/kafka
kubectl delete svc/zookeeper
kubectl delete svc/kafka-manager

kubectl delete ing/ingress-pamproducer

# deploy kafka and zookeeper
kubectl apply -f ./k8s-deploy/kafka/zookeeper-deployment.yaml
kubectl apply -f ./k8s-deploy/kafka/zookeeper-service.yaml
kubectl apply -f ./k8s-deploy/kafka/kafka-deployment.yaml
kubectl apply -f ./k8s-deploy/kafka/kafka-service.yaml
kubectl apply -f ./k8s-deploy/kafka/kafka-manager-deployment.yaml
kubectl apply -f ./k8s-deploy/kafka/kafka-manager-service.yaml

# deploy producer and consumer
kubectl apply -f ./k8s-deploy/producer/producer-deployment.yaml
kubectl apply -f ./k8s-deploy/producer/producer-service.yaml
kubectl apply -f ./k8s-deploy/producer/producer-ingress.yaml

kubectl apply -f ./k8s-deploy/consumer/consumer-deployment.yaml
kubectl apply -f ./k8s-deploy/consumer/consumer-service.yaml
